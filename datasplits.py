import numpy as np
import networkx as nx
from random import sample,seed

from collections import defaultdict
import copy
import os

import torch
import torch.nn as nn
import time
import pickle as  pkl
import scipy.sparse as ssp
from sklearn.metrics import roc_auc_score, average_precision_score
from tabulate import tabulate
def generate_query_nodes(nx_graph):
    q_num_triangles=nx.algorithms.cluster.triangles(nx_graph)
    nodes_num_triangles=[(k,v) for k,v in q_num_triangles.items()]
    query_nodes={q:v for q,v in nodes_num_triangles}
    return query_nodes


def populate_query_set(nx_graph,query_nodes,tmin=1):
    query_nodes=[k for k in query_nodes if query_nodes[k]>=tmin]
    query_node_to_neighs_dict={q:set(nx.neighbors(nx_graph,q))-{q} for q in query_nodes}
    query_node_to_2nd_neighs_union_1st_neighs_dict={q:set.union(*[set(nx.neighbors(nx_graph,i))|{i} for i in nx.neighbors(nx_graph,q)])|
                                       query_node_to_neighs_dict[q] for q in query_nodes}
    query_node_to_2nd_neighs_and_not_1st_neighs_dict={q:(query_node_to_2nd_neighs_union_1st_neighs_dict[q] -
                                                        query_node_to_neighs_dict[q])-{q} for q in query_nodes}

    query_full_set = {}
    for q_node in query_nodes:
        query_full_set[q_node] = {}
        query_full_set[q_node]['Nbr'] = list(query_node_to_neighs_dict[q_node])
        query_full_set[q_node]['NonNbr']=list(query_node_to_2nd_neighs_and_not_1st_neighs_dict[q_node])
    assert all(set(query_full_set[q_node]['Nbr']).intersection(query_full_set[q_node]['NonNbr']) == set() for q_node in query_nodes)
    assert all(len(query_full_set[q_node]['Nbr']) + len(query_full_set[q_node]['NonNbr'])==\
           len(query_node_to_2nd_neighs_union_1st_neighs_dict[q_node]-{q_node}) for q_node in query_nodes)
    assert all(q_node not in set(query_full_set[q_node]['Nbr']) for q_node in query_nodes)
    assert all(q_node not in set(query_full_set[q_node]['NonNbr']) for q_node in query_nodes)
    return query_full_set

def split_query_set(split_frac,query_nodes,query_full_set):
    query_training_set = {}
    query_test_set = {}
    for q_node in query_nodes:
        query_training_set[q_node] = {}
        query_test_set[q_node]     = {}
        for key in ['Nbr','NonNbr'] :
            test_len = int(split_frac * len(query_full_set[q_node][key]))
            query_test_set[q_node][key] = sample(query_full_set[q_node][key],test_len)
            query_training_set[q_node][key] = list(set(query_full_set[q_node][key]) \
                                                   - set(query_test_set[q_node][key]))

    return  query_training_set, query_test_set

def generate_edge_lists(nx_graph,query_nodes,query_training_set, query_test_set):

    list_test_edges = []
    list_test_non_edges = []
    list_training_edges = []
    list_training_non_edges = []
    training_edge_set=set()
    training_ne_set=set()
    test_edge_set=set()
    test_ne_set=set()

    for q_node in query_nodes:
        for x in query_test_set[q_node]['Nbr']:
            if (x,q_node) not in test_edge_set:
                test_edge_set.add((q_node,x))
            # list_test_edges.append((q_node, x))
        for x in query_test_set[q_node]['NonNbr']:
            if (x, q_node) not in test_ne_set:
                test_ne_set.add((q_node, x))
            # list_test_non_edges.append((q_node, x))

    test_mask=[[],[]]
    for q_node in query_nodes:
        for x in query_test_set[q_node]['Nbr']:
            test_mask[0].append(q_node)
            test_mask[1].append(x)
    test_mask_csr=ssp.csr_matrix(([1]*len(test_mask[0]),test_mask),shape=(nx_graph.number_of_nodes(),
                                                                          nx_graph.number_of_nodes()))
    for q_node in query_nodes:
        templist = copy.deepcopy(query_training_set[q_node]['Nbr'])
        for x in templist :
            if test_mask_csr[x,q_node]!=1: #not checking for test_mask_csr[q_node,x] because for q_node, train and test are disjoint
                if (x, q_node) not in training_edge_set:
                    training_edge_set.add((q_node, x))
            else :
                query_training_set[q_node]['Nbr'].remove(x)

    test_mask=[[],[]]
    for q_node in query_nodes:
        for x in query_test_set[q_node]['NonNbr']:
            test_mask[0].append(q_node)
            test_mask[1].append(x)
    test_mask_csr=ssp.csr_matrix(([1]*len(test_mask[0]),test_mask),shape=(nx_graph.number_of_nodes(),
                                                                          nx_graph.number_of_nodes()))

    for q_node in query_nodes:
        templist = copy.deepcopy(query_training_set[q_node]['NonNbr'])
        for x in  templist:
            if test_mask_csr[x,q_node]!=1:
                if (x, q_node) not in training_ne_set:
                    training_ne_set.add((q_node, x))
                # list_training_non_edges.append((q_node, x))
            else : query_training_set[q_node]['NonNbr'].remove(x)
    print("done4")

    for u,v in training_edge_set: list_training_edges.append((min(u,v),max(u,v)))
    for u,v in training_ne_set: list_training_non_edges.append((min(u,v),max(u,v)))
    for u,v in test_edge_set: list_test_edges.append((min(u,v),max(u,v)))
    for u,v in test_ne_set: list_test_non_edges.append((min(u,v),max(u,v)))

    return list_training_edges,list_training_non_edges,list_test_edges,list_test_non_edges


def prepare_splits(av,A_csr):
    nx_graph=nx.from_scipy_sparse_matrix(A_csr)
    query_nodes = generate_query_nodes(nx_graph)
    query_full_set =  populate_query_set(nx_graph,query_nodes,av.tmin)
    query_nodes=list(query_full_set.keys())

    query_training_set, query_test_set =  split_query_set(av.tfrac,query_nodes,query_full_set)
    list_training_edges, list_training_non_edges, list_test_edges, list_test_non_edges \
        = generate_edge_lists(nx_graph,query_nodes,query_training_set, query_test_set)

    if av.vfrac==0:
        list_val_edges = []
        list_val_non_edges = []
    else :
        query_training_only_set, query_val_set =  split_query_set(av.vfrac,query_nodes,query_training_set)
        list_training_edges, list_training_non_edges, list_val_edges, list_val_non_edges \
            = generate_edge_lists(nx_graph,query_nodes,query_training_only_set, query_val_set)
    total_len_edge=len(list_training_edges)+len(list_test_edges)+len(list_val_edges)
    total_len_non_edge=len(list_training_non_edges)+len(list_test_non_edges)+len(list_val_non_edges)
    table=[[(1-av.tfrac)*(1-av.vfrac),len(list_training_edges)/total_len_edge,len(list_training_non_edges)/total_len_non_edge],
           [(1 - av.tfrac) * (av.vfrac), len(list_val_edges) / total_len_edge,
            len(list_val_non_edges) / total_len_non_edge],
           [av.tfrac, len(list_test_edges) / total_len_edge,
            len(list_test_non_edges) / total_len_non_edge]]
    if av.print_data==1:
        print(tabulate(table,headers=["intended frac","actual pos frac","actual neg frac"]))
        exit(0)

    return query_nodes, \
           list_training_edges, \
           list_training_non_edges, \
           list_test_edges, \
           list_test_non_edges, \
           list_val_edges, \
           list_val_non_edges

def get_splits_from_file(args,A):
    file=args.splitfile
    if file is None:
        file=f"Data/{args.dataset}_tfrac_{args.tfrac}_vfrac_{args.vfrac}_tmin_{args.tmin}"
    if not os.path.exists(file):
        print("--------file not found----------")
        x=prepare_splits(args,A)
        with open(file,"wb") as ofile:
            pkl.dump(x,ofile)
        return x
    else:
        with open(file,"rb") as ofile:
            x=pkl.load(ofile)
        return x
