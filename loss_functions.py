import torch,numpy as np
from tqdm import  tqdm


def get_model_output(args,model,data,wholegraph):
    x, edge_index, batch, edge_weight, node_id, uvs_index, ugraphs, vgraphs = [None] * 8
    if args.uv_data == "subgraph" and args.input == "combined":
        data = data.to(args.device)
        y = data.y
        x = data.z
        edge_index = data.edge_index
        batch = data.batch
        edge_weight = data.edge_weight
        node_id = data.node_id
    elif args.uv_data == "subgraph" and args.input == "ind_nodes":
        ugraphs, vgraphs, y= data
        ugraphs = ugraphs.to(args.device)
        vgraphs = vgraphs.to(args.device)
        # num_nodes= num_nodes.to(args.device)
        y=y.to(args.device)
    elif args.uv_data == "wholegraph":
        # remember when it is wholegraph it is only in "ind_nodes" setting
        uvs_index, y = data
        edge_index = wholegraph.edge_index.to(args.device)
        node_id = wholegraph.node_id.to(args.device)
        uvs_index = uvs_index.to(args.device)
        y = y.to(args.device)
    return model(x=x, edge_index=edge_index, batch=batch, edge_weight=edge_weight,
                   node_id=node_id, ugraphs=ugraphs, vgraphs=vgraphs, uvs_index=uvs_index),y


def bce_loss(args,loader,model,optimizer,wholegraph,mode):
    # pbar = tqdm(loader)
    total_loss=0
    total_count=0
    for data in loader:
        logits,y=get_model_output(args,model,data,wholegraph)
        loss = torch.nn.BCEWithLogitsLoss()(logits.view(-1), y.to(torch.float))
        total_loss += loss.item() * len(y)
        total_count+=len(y)
        if mode=="train":
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()
    return total_loss/total_count

def pairwise_ranking_loss(pos,neg, margin):
    n_1 = pos.shape[0]
    n_2 = neg.shape[0]
    pos = pos.unsqueeze(1).expand(n_1, n_2)
    neg = neg.unsqueeze(0).expand(n_1, n_2) + margin
    pos = neg - pos
    hinge = torch.nn.ReLU()
    pos = hinge(pos)
    return torch.sum(pos)

def ranking_loss(args,qnodes_dl_dict,model,optimizer,qnode2posneg,wholegraph,mode):
    model.train()
    all_qnodes=list(qnode2posneg.keys())
    splits=np.array_split(np.random.permutation(all_qnodes),5)
    total_loss=0
    for qnodes_in_this_batch in splits:
        batch_loss=torch.tensor(0.).to(args.device)
        for qnode in qnodes_in_this_batch:
            qnodes_pos_dl=qnodes_dl_dict[qnode]["pos"]
            qnodes_neg_dl=qnodes_dl_dict[qnode]["neg"]
            pos_scores = []
            neg_scores = []
            for data in qnodes_pos_dl:
                logits,_ = get_model_output(args, model, data, wholegraph)
                pos_scores.append(logits)
            for data in qnodes_neg_dl:
                logits,_ = get_model_output(args, model, data, wholegraph)
                neg_scores.append(logits)

            loss=pairwise_ranking_loss(torch.cat(pos_scores,dim=0),torch.cat(neg_scores,dim=0),args.margin)
            batch_loss+=loss
        total_loss+=batch_loss.item()
        if mode == "train":
            optimizer.zero_grad()
            batch_loss.backward()
            optimizer.step()

    return total_loss/len(all_qnodes)














