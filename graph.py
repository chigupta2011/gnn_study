import networkx as nx
import scipy.sparse as ssp,numpy as np

class MyGraph(object):
    def __init__(self,args):
        self.dataset=args.daataset

    def fill_graph_with_data(self,args,dataset):
        if not (dataset == "cora" or dataset == 'citeseer'):
            cites_path = f"/mnt/infonas/data/baekgupta/permgnn/Datasets/{dataset}.txt"
            gr = nx.convert_node_labels_to_integers(nx.read_edgelist(cites_path))
            self.A_csr=nx.to_scipy_sparse_matrix(gr)
            self.node_information=np.identity(gr.number_of_nodes())
            self.num_nodes=A_csr.shape[0]
            self.num_features
        else:
            cites_path = f"/mnt/infonas/data/baekgupta/permgnn/Datasets/{dataset}.cites"
            content_path = f"/mnt/infonas/data/baekgupta/permgnn/Datasets/{dataset}.content"
            node_map = {}
            node_to_idx_dict = {}
            idx_to_node_dict = {}
            label_map = {}
            labels = {}
            node_information = []
            with open(content_path, "r") as fp:
                for i, line in enumerate(fp):
                    info = line.strip().split()
                    # self.node_features [i,:] = [float(x) for x in info[1:-1]]
                    idx_to_node_dict[i] = info[0]
                    node_to_idx_dict[info[0]] = i
                    node_map[info[0]] = i
                    node_information.append(list(map(float, info[1:-1])))
                    if not info[-1] in label_map:
                        label_map[info[-1]] = len(label_map)
                    labels[i] = label_map[info[-1]]
            num_nodes = len(node_map)
            # adjacency_list={i:set() for i in range(num_nodes)}
            original_edges_list = []
            with open(cites_path, "r") as fp:
                for i, line in enumerate(fp):
                    info = line.strip().split()
                    # Problem in "citeseer.cites" There are 17 edges referring nodes, not mentioned in citeseer.content
                    if (info[0] not in node_map or info[1] not in node_map):
                        continue
                    node1 = node_map[info[0]]
                    node2 = node_map[info[1]]
                    original_edges_list.append((node1, node2))
            A = ssp.csr_matrix(([1] * len(original_edges_list), list(zip(*original_edges_list))),
                               shape=(num_nodes, num_nodes))
            A[range(num_nodes), range(num_nodes)] = 0


