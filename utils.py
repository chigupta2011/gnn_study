import os,torch
import scipy.sparse as ssp
import numpy as np
import  networkx as nx
import  pickle as pkl
from datasplits import prepare_splits
from torch_geometric.nn import Node2Vec

def process_args(args):
    # args.skip_cnn1 = args.skip_cnn1 or args.skip_cnn
    # args.skip_cnn2 = args.skip_cnn2 or args.skip_cnn
    # args.skip_maxpool = args.skip_maxpool or args.skip_cnn
    args.cuda = torch.cuda.is_available() and args.cuda_device>=0
    args.device = "cuda" if args.cuda else "cpu"
    if args.hop != 'auto':
        args.hop = int(args.hop)
    if args.cuda_device >= 0:
        torch.cuda.set_device(args.cuda_device)

    if args.hidden_channels[0]==-1:
        args.hidden_channels=[8,16, 32, 64, 128, 256]
    if args.num_layers[0]==-1:
        args.num_layers=[2,3,4,5,6,7,8]
    if args.last_hidden_size[0]==-1:
        args.last_hidden_size=[8,16,32,64,128,256]

    # if args.max_nodes_per_hop is not None:
    #     args.max_nodes_per_hop = int(args.max_nodes_per_hop)
    # args.file_dir = os.path.dirname(os.path.realpath('__file__'))
    # return args



def get_A_from_cites_file(dataset):
    if not (dataset=="cora" or dataset=='citeseer'):
        cites_path=f"/mnt/infonas/data/baekgupta/permgnn/Datasets/{dataset}.txt"
        gr = nx.convert_node_labels_to_integers(nx.read_edgelist(cites_path))
        #node itself is considered in neighbourhood set
        return nx.to_scipy_sparse_matrix(gr),np.identity(gr.number_of_nodes())
    else:
        cites_path=f"/mnt/infonas/data/baekgupta/permgnn/Datasets/{dataset}.cites"
        content_path=f"/mnt/infonas/data/baekgupta/permgnn/Datasets/{dataset}.content"
        node_map={}
        node_to_idx_dict={}
        idx_to_node_dict={}
        label_map={}
        labels={}
        node_information=[]
        with open(content_path,"r") as fp:
            for i,line in enumerate(fp):
                info = line.strip().split()
                # self.node_features [i,:] = [float(x) for x in info[1:-1]]
                idx_to_node_dict[i] = info[0]
                node_to_idx_dict[info[0]] = i
                node_map[info[0]] = i
                node_information.append(list(map(float,info[1:-1])))
                if not info[-1] in label_map:
                    label_map[info[-1]] = len(label_map)
                labels[i] = label_map[info[-1]]
        num_nodes=len(node_map)
        # adjacency_list={i:set() for i in range(num_nodes)}
        original_edges_list=[]
        with open(cites_path,"r") as fp:
            for i,line in enumerate(fp):
                info = line.strip().split()
                #Problem in "citeseer.cites" There are 17 edges referring nodes, not mentioned in citeseer.content
                if (info[0] not in node_map or info[1] not in node_map) :
                    continue
                node1 = node_map[info[0]]
                node2 = node_map[info[1]]
                # adjacency_list[node1].add(node2)
                # adjacency_list[node2].add(node1)
                original_edges_list.append((node1, node2))
        # A=np.zeros((num_nodes,num_nodes))
        A=ssp.csr_matrix(([1]*len(original_edges_list),list(zip(*original_edges_list))),shape=(num_nodes,num_nodes))
        A[range(num_nodes),range(num_nodes)]=0
        return A,np.array(node_information,dtype=float)
