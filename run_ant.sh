#!/bin/bash
dataset=$1
if [[ $dataset = "cora" ]]
then
    tfrac=0.24
    vfrac=0.20
    waittime=500
    useattrs=( 0 1 )

elif [[ $dataset = "citeseer" ]]
then
    tfrac=0.24
    vfrac=0.20
    waittime=300
    useattrs=( 0 1 )
elif [[ $dataset = "Twitter_3" ]]
then
    tfrac=0.115
    vfrac=0.08
    waittime=150
    useattrs=( 0 1 )
elif [[ $dataset = "Gplus_1" ]]
then
    tfrac=0.11
    vfrac=0.08
    useattrs=( 0 1 )
elif [[ $dataset = "PB" ]]
then
    tfrac=0.12
    vfrac=0.09
    useattrs=( 0 1 )
elif [[ $dataset = "Twitch_PT" ]]
then
    tfrac=0.12
    vfrac=0.08
    useattrs=( 0 1 )
elif [[ $dataset = "Twitter_2" ]]
then
    tfrac=0.11
    vfrac=0.07
    useattrs=( 0 1 )
elif [[ $dataset = "Twitter_1" ]]
then
    tfrac=0.11
    vfrac=0.07
    useattrs=( 0 1 )
fi
batch_size=64
eval_metric='per_query'
#run="run5_patience10_AllMargins_params"
run=$2
base_dir=logs/${dataset}_${tfrac}_${vfrac}_${run}
mkdir -p ${base_dir}
pids=""
#uv_data="wholegraph"
uv_data="subgraph"
for model in {"GCN","SAGE","GIN","DGCNN"} ; do
#  for loss in {'bce'} ; do
  loss=bce
    for use_n2v in {2,} ; do
      for use_attribute in ${useattrs[@]}; do
          for use_tag in {0,1}; do
              for input in {"ind_nodes","combined"}; do
                        if [[ $use_attribute -eq 0 ]] && [[ $use_n2v -eq 0 ]] && [[ $use_tag -eq 0 ]]
                        then
                          continue
                        fi;
                        if [ $uv_data = "wholegraph" ] && [ $use_tag -eq 1 ]; then
                          continue
                        fi;
                        if [ $uv_data = "wholegraph" ] && [ $input = "combined" ]; then
                          continue
                        fi;
                        if [ $uv_data = "wholegraph" ] && [ $model = "DGCNN" ]; then
                          continue
                        fi;
                        id=$3
                        count=0
                        free_mem=$(nvidia-smi --query-gpu=memory.free --format=csv -i $id | grep -Eo [0-9]+)
#                        while [ $free_mem -lt 4000 ]; do
                        while [[ ($free_mem -lt 8000  && $id -eq 0) || ($free_mem -lt 3000 && $id -ne 0) ]]; do
                            if [[ $id -eq 0 ]]
                            then
                                id=1
                            elif [[ $id -eq 1 ]]
                            then
                                id=2
                            else
                                id=1
                            fi
                            free_mem=$(nvidia-smi --query-gpu=memory.free --format=csv -i $id | grep -Eo [0-9]+)

                            if [[ $count -eq 20 ]]
                            then
                                echo $pids
#                                wait $pids || { echo "there were errors" >&2; } #exit 1; }
                                pids=""
                            fi;

                            count=$[$count +1]
                            sleep 5
                        done


                        python main.py --dataset ${dataset} --batch_size ${batch_size}  --tfrac ${tfrac} --vfrac ${vfrac} \
                          --use_n2v ${use_n2v} --use_attribute ${use_attribute} --use_tag ${use_tag} \
                          --uv_data ${uv_data} --input ${input} --model ${model} --loss ${loss}  --cuda_device ${id}  \
                          --hidden_channels 32 --num_layers 3 --last_hidden_size 128 --reqd_params -1 --margin -1\
                       > ${base_dir}/${uv_data}_${input}_${use_n2v}${use_attribute}${use_tag}_${loss}_${model}_${eval_metric}_-1 2>&1 &

                       pids+=" $!"
                       sleep 120
              done
          done
      done
    done
#  done
done
wait $pids || { echo "there were errors" >&2; } #exit 1; }
pids=""
echo completed the first set of expts
echo "##############################################################"
echo "##############################################################"
exit 0
#uv_data="subgraph"
for model in {"GCN","SAGE","GIN","DGCNN"} ; do
#for model in {"GIN",} ; do
  loss=ranking
    for use_n2v in {0,1} ; do
      for use_attribute in ${useattrs[@]}; do
          for use_tag in {0,1}; do
              for input in {"ind_nodes","combined"}; do
                  for margin in {0.1,1,10}; do
                        if [[ $use_attribute -eq 0 ]] && [[ $use_n2v -eq 0 ]] && [[ $use_tag -eq 0 ]]
                        then
                          continue
                        fi;
                        if [ $uv_data = "wholegraph" ] && [ $use_tag -eq 1 ]; then
                          continue
                        fi;
                        if [ $uv_data = "wholegraph" ] && [ $input = "combined" ]; then
                          continue
                        fi;
                        if [ $uv_data = "wholegraph" ] && [ $model = "DGCNN" ]; then
                          continue
                        fi;
                        id=$3
                        count=0
                        free_mem=$(nvidia-smi --query-gpu=memory.free --format=csv -i $id | grep -Eo [0-9]+)
#                        while [ $free_mem -lt 4000 ]; do
                        while [[ ($free_mem -lt 8000  && $id -eq 0) || ($free_mem -lt 3000 && $id -ne 0) ]]; do
                            if [[ $id -eq 0 ]]
                            then
                                id=1
                            elif [[ $id -eq 1 ]]
                            then
                                id=2
                            else
                                id=1
                            fi
                            free_mem=$(nvidia-smi --query-gpu=memory.free --format=csv -i $id | grep -Eo [0-9]+)

                            if [[ $count -eq 15 ]]
                            then
                                echo $pids
#                                wait $pids || { echo "there were errors" >&2; } #exit 1; }
                                pids=""
                            fi;

                            count=$[$count +1]
                            sleep 5
                        done
                        python main.py --dataset ${dataset} --batch_size ${batch_size}  --tfrac ${tfrac} --vfrac ${vfrac} \
                          --use_n2v ${use_n2v} --use_attribute ${use_attribute} --use_tag ${use_tag} \
                          --uv_data ${uv_data} --input ${input} --model ${model} --loss ${loss}  --cuda_device ${id}  \
                          --hidden_channels 32 --num_layers 3 --last_hidden_size 128 --reqd_params -1 --margin ${margin} \
                       > ${base_dir}/${uv_data}_${input}_${use_n2v}${use_attribute}${use_tag}_${loss}_${model}_${eval_metric}_${margin} 2>&1 &

                       pids+=" $!"
                       sleep 120
              done
          done
      done
    done
  done
done
wait $pids || { echo "there were errors" >&2; } #exit 1; }
pids=""
echo completed the second set of expts
echo "##############################################################"
echo "##############################################################"

#
exit 0
#params=$3
#id=$4
#model=$7
#          if [ ${uv_data} == "wholegraph" ] && [ ${model} == "DGCNN" ]; then
#            continue
#          fi
#          for loss in ranking bce
#          do
#            for use_n2v in 0 1
#            do
#                for use_tag in 0 1
#                do
#                  for input in ind_nodes combined
#                  do
#                    for margin in 0.1 1 10
#                    do
#                        if [[ $use_attribute -eq 0 ]] && [[ $use_n2v -eq 0 ]] && [[ $use_tag -eq 0 ]]
#                        then
#                          continue
#                        fi;
#                        while :
#                        do
#                            free_mem0=$(nvidia-smi --query-gpu=memory.free --format=csv -i $id | grep -Eo [0-9]+)
#                            if [ $free_mem0 -gt 5000 ];
#                            then
#                                break
#                            fi
#                            sleep 1
#                        done
#                        echo ${base_dir}/${uv_data}_${input}_${use_n2v}${use_attribute}${use_tag}_${loss}_${model}_${eval_metric}_${margin}_${params}
#                        echo "----------------------------------------------------"
#                        echo "----------------------------------------------------"
#                        timetowait=3
#                        count=0
#
#                        python main.py --dataset ${dataset} --batch_size ${batch_size}  --tfrac ${tfrac} --vfrac ${vfrac} \
#                          --use_n2v ${use_n2v} --use_attribute ${use_attribute} --use_tag ${use_tag} \
#                          --uv_data ${uv_data} --input ${input} --model ${model} --loss ${loss}  --cuda_device ${id} --margin ${margin} \
#                          --hidden_channels -1 --num_layers -1 --last_hidden_size -1 --reqd_params ${params} \
#                          > ${base_dir}/${uv_data}_${input}_${use_n2v}${use_attribute}${use_tag}_${loss}_${model}_${eval_metric}_${margin}_${params} 2>&1
##                          > ${base_dir}/${uv_data}_${input}_${use_n2v}${use_attribute}${use_tag}_${loss}_${model}_${eval_metric}_${margin}_${params} 2>&1 &
#
#                        while [[ $? -gt 0 ]];
#                            do
#                            count=$((count+1))
#                            sleep ${timetowait}
#                            echo doing currently ${count} iteration
#                            python main.py --dataset ${dataset} --batch_size ${batch_size}  --tfrac ${tfrac} --vfrac ${vfrac} \
#                              --use_n2v ${use_n2v} --use_attribute ${use_attribute} --use_tag ${use_tag} \
#                              --uv_data ${uv_data} --input ${input} --model ${model} --loss ${loss}  --cuda_device ${id} --margin ${margin} \
#                              --hidden_channels -1 --num_layers -1 --last_hidden_size -1 --reqd_params ${params} \
#                              > ${base_dir}/${uv_data}_${input}_${use_n2v}${use_attribute}${use_tag}_${loss}_${model}_${eval_metric}_${margin}_${params} 2>&1
#
#                            done
#                        pids+=" $!"
#                    done
#                  done
#                done
#            done
#           done
#exit 0


#for model in {"DGCNN","GCN","SAGE","GIN"} ; do
#  for loss in {'bce','ranking'} ; do
#    for use_n2v in {0,1} ; do
#      for use_attribute in {0,1} ; do
#        for use_tag in {0,1} ; do
#          for input in {"ind_nodes","combined"} ; do
#
#            if [[ $use_attribute -eq 0 ]] && [[ $use_n2v -eq 0 ]] && [[ $use_tag -eq 0 ]]
#            then
#              continue
#            fi;
#            id=0
#            count=0
#            free_mem=$(nvidia-smi --query-gpu=memory.free --format=csv -i $id | grep -Eo [0-9]+)
#            while [ $free_mem -lt 2000 ]; do
#                id=$(( count % 3 ))
#                free_mem=$(nvidia-smi --query-gpu=memory.free --format=csv -i $id | grep -Eo [0-9]+)
#
#                if [[ $count -eq 3 ]]
#                then
#                    echo $pids
#                    wait $pids || { echo "there were errors" >&2; } #exit 1; }
#                    pids=""
#                fi;
#
#                count=$[$count +1]
#                sleep 5
#            done
#
#
#            python main.py --dataset ${dataset} --batch_size ${batch_size}   --tfrac ${tfrac} --vfrac ${vfrac} \
#               --use_n2v ${use_n2v} --use_attribute ${use_attribute} --use_tag ${use_tag} \
#               --uv_data "subgraph" --input ${input} --model ${model} --loss ${loss}  --cuda_device ${id}\
#               > ${base_dir}/subgraph_${input}_${use_n2v}${use_attribute}${use_tag}_${loss}_${model}_${eval_metric} 2>&1 &
#            pids+=" $!"
#            sleep 35
#          done
#        done
#      done
#    done
#  done
#done
#
