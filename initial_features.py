import os,numpy as np,networkx as nx
import scipy.sparse as ssp
import  torch
from torch_geometric.nn import  Node2Vec
from evaluation_protocols import  compute_scores
from early_stopping import EarlyStopping
def get_n2v_embeddings(args,A_csr,val_pos_dl,val_neg_dl,val_query2posneg,dummy):
    embedding_dim=64
    if dummy:
        return torch.zeros(A_csr.shape[0],embedding_dim)
    if args.n2v_file is None:
        args.n2v_file=f"N2V/{args.dataset}_tfrac_{args.tfrac}_vfrac_{args.vfrac}_tmin_{args.tmin}"
    if not os.path.exists(args.n2v_file):
        print("------------n2v file not found-----------")
        local_device = args.device
        num_nodes=A_csr.shape[0]
        us, vs, _ = ssp.find(A_csr)
        # edge_index = torch.tensor([np.concatenate((us, vs)), np.concatenate((vs, us))], dtype=torch.long).to(
        #     local_device)
        edge_index = torch.tensor([us,vs], dtype=torch.long).to(
            local_device)
        model = Node2Vec(edge_index, embedding_dim=embedding_dim, walk_length=max(5,int(num_nodes*0.05)),
                         context_size=2, walks_per_node=10,
                         num_negative_samples=5, p=0.5, q=0.5, sparse=True, num_nodes=num_nodes).to(local_device)
        # model = Node2Vec(edge_index, embedding_dim=64, walk_length=max(10,int(num_nodes*0.02)),
        #                  context_size=5, walks_per_node=10,
        #                  num_negative_samples=5, p=1, q=1, sparse=True, num_nodes=num_nodes).to(local_device)
        loader = model.loader(batch_size=64, shuffle=True, num_workers=30)
        local_optimizer = torch.optim.SparseAdam(list(model.parameters()), lr=0.1)

        def train():
            model.train()
            total_loss = 0
            for pos_rw, neg_rw in loader:
                local_optimizer.zero_grad()
                loss = model.loss(pos_rw.to(local_device), neg_rw.to(local_device))
                loss.backward()
                local_optimizer.step()
                total_loss += loss.item()
            return total_loss / len(loader)
        def eval_local():
            model.eval()
            with torch.no_grad():
                embeddings = model()
                pos_scores = torch.nn.CosineSimilarity()(embeddings[[u for u,v in val_pos_dl]],embeddings[[v for u,v in val_pos_dl]])
                neg_scores = torch.nn.CosineSimilarity()(embeddings[[u for u,v in val_neg_dl]],embeddings[[v for u,v in val_neg_dl]])
                return compute_scores(args,pos_scores.cpu().numpy(),
                                                   neg_scores.cpu().numpy(),
                                                   val_query2posneg)

        auc, ap, qmap, qmrr = eval_local()
        if args.eval_metric == "per_query":
            compare_with = qmap
        else:
            compare_with = ap
        earlystopping = EarlyStopping(compare_with,patience=5)

        for epoch in range(100):
            train()
            auc, ap, qmap, qmrr = eval_local()
            if args.eval_metric == "per_query":
                compare_with = qmap
            else:
                compare_with = ap
            earlystopping(compare_with, epoch, model)
            if earlystopping.early_stop:
                break

            # print(f"loss {loss}")
        model.eval()
        with torch.no_grad():
            embeddings = model().cpu()
        # exit(0)
        torch.save(embeddings, args.n2v_file)
        # exit(0)
        return embeddings
    else:
        print("------------n2v file found----------")
        return torch.load(args.n2v_file)

def get_nmf_embeddings(args,A_csr,val_pos_dl,val_neg_dl,val_query2posneg,train_pos,train_neg,dummy):
    embedding_dim=64
    if dummy:
        return torch.zeros(A_csr.shape[0],embedding_dim)
    if args.nmf_file is None:
        args.nmf_file=f"NMF/{args.dataset}_tfrac_{args.tfrac}_vfrac_{args.vfrac}_tmin_{args.tmin}"
    if not os.path.exists(args.nmf_file):
        print("------------nmf file not found-----------")
        local_device = args.device
        us, vs, _ = ssp.find(A_csr)
        A=torch.tensor(A_csr.toarray()).float().to(local_device)
        u,s,v=torch.svd(A)
        u=u[:,:embedding_dim]@torch.diag(torch.sqrt(s[:embedding_dim]))
        embeddings=torch.nn.Parameter(u.cuda(),requires_grad=True)
        pos_plus_neg=len(us)+len(train_neg[0])
        lambda_=10
        optimizer = torch.optim.Adam([embeddings], lr=args.lr)
        earlystopping = EarlyStopping(-np.inf,patience=10)
        def eval_local(embeddings):
            with torch.no_grad():
                pos_scores = torch.sum(embeddings[[u for u,v in val_pos_dl]]*embeddings[[v for u,v in val_pos_dl]],dim=-1)
                neg_scores = torch.sum(embeddings[[u for u,v in val_neg_dl]]*embeddings[[v for u,v in val_neg_dl]],dim=-1)
                reco_loss=1/pos_plus_neg*(torch.sum(torch.square(pos_scores- 1)) +
                                          torch.sum(torch.square(neg_scores - 0)))
                regul_loss=lambda_*torch.mean(torch.square(embeddings))

            return reco_loss+regul_loss,compute_scores(args,pos_scores.cpu().numpy(),neg_scores.cpu().numpy(),val_query2posneg)

        train_neg_0=list(train_neg[0])
        train_neg_1=list(train_neg[1])
        print((embeddings[us]*embeddings[vs]).shape)
        for epoch in range(1000):
            reco_loss=1/pos_plus_neg*(torch.sum(torch.square(torch.sum(embeddings[us]*embeddings[vs],dim=-1)- 1)) +
                                      torch.sum(torch.square(torch.sum(embeddings[train_neg_0] * embeddings[train_neg_1], dim=-1) - 0)))
            regul_loss=lambda_*torch.mean(torch.square(embeddings))
            loss=reco_loss+regul_loss
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()
            val_loss,(auc, ap, qmap, qmrr) = eval_local(embeddings)
            compare_with=-val_loss
            earlystopping(compare_with, epoch, embeddings)
            if earlystopping.early_stop:
                break
        print(eval_local(embeddings))
        embeddings = embeddings.cpu()
        torch.save(embeddings, args.nmf_file)
        # exit(0)
        return embeddings
    else:
        print("------------nmf file found----------")
        return torch.load(args.nmf_file)

def get_freezed_embed(args,A_csr,intrinsic_features,val_pos_dl,val_neg_dl,val_quer2posneg,train_pos,train_neg,dummy=False):
    num_nodes=A_csr.shape[0]
    features=torch.empty(num_nodes,0)
    if args.use_attribute:
        features=torch.cat((torch.tensor(intrinsic_features).float(),features),dim=1)
    if args.use_n2v==1:
        features=torch.cat((get_n2v_embeddings(args,A_csr,val_pos_dl,val_neg_dl,val_quer2posneg,dummy),features),dim=1)
    if args.use_n2v==2:
        features=torch.cat((get_nmf_embeddings(args,A_csr,val_pos_dl,val_neg_dl,val_quer2posneg,train_pos,train_neg,dummy),features),dim=1)
    return features




