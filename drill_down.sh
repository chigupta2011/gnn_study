#! /bin/bash
id=0
count=0

free_mem=$(nvidia-smi --query-gpu=memory.free --format=csv -i $id | grep -Eo [0-9]+)
while [ $free_mem -lt 6000 ]; do
    id=$(( count % 3 ))
    free_mem=$(nvidia-smi --query-gpu=memory.free --format=csv -i $id | grep -Eo [0-9]+)

    if [[ $count -eq 3 ]]
    then
        id=0
        count=0
        sleep 2m
    fi;

    count=$[$count +1]
    sleep 5
done
python main.py --dataset Gplus_1 --batch_size 64   --tfrac 0.11 --vfrac 0.08              --use_n2v 0 --use_attribute 0  --use_tag 1              --uv_data  subgraph --input combined --model "GCN" --loss "ranking"  --hidden_channels 32 --num_layers 3 --last_hidden_size 128 --reqd_params -1 --margin 10.0  --eval_metric per_query --cuda_device ${id} >             ./drill_down/Gplus_1_0.11_0.08_subgraph_combined_001_ranking_GCN_per_query_10.0_drilldown 2>&1 &

echo $! $id $count ; sleep 4m

free_mem=$(nvidia-smi --query-gpu=memory.free --format=csv -i $id | grep -Eo [0-9]+)
while [ $free_mem -lt 6000 ]; do
    id=$(( count % 3 ))
    free_mem=$(nvidia-smi --query-gpu=memory.free --format=csv -i $id | grep -Eo [0-9]+)

    if [[ $count -eq 3 ]]
    then
        id=0
        count=0
        sleep 2m
    fi;

    count=$[$count +1]
    sleep 5
done
python main.py --dataset Gplus_1 --batch_size 64   --tfrac 0.11 --vfrac 0.08              --use_n2v 1 --use_attribute 0 --use_tag 1             --uv_data  subgraph --input combined --model "GCN" --loss "ranking"  --hidden_channels 32 --num_layers 3 --last_hidden_size 128 --reqd_params -1 --margin 10.0  --eval_metric per_query --cuda_device ${id} >             ./drill_down/Gplus_1_0.11_0.08_subgraph_combined_101_ranking_GCN_per_query_10.0_drilldown 2>&1 &

echo $! $id $count ; sleep 4m

free_mem=$(nvidia-smi --query-gpu=memory.free --format=csv -i $id | grep -Eo [0-9]+)
while [ $free_mem -lt 6000 ]; do
    id=$(( count % 3 ))
    free_mem=$(nvidia-smi --query-gpu=memory.free --format=csv -i $id | grep -Eo [0-9]+)

    if [[ $count -eq 3 ]]
    then
        id=0
        count=0
        sleep 2m
    fi;

    count=$[$count +1]
    sleep 5
done
python main.py --dataset Gplus_1 --batch_size 64   --tfrac 0.11 --vfrac 0.08              --use_n2v 0 --use_attribute 0  --use_tag 1              --uv_data  subgraph --input ind_nodes --model "GIN" --loss "ranking"  --hidden_channels 32 --num_layers 3 --last_hidden_size 128 --reqd_params -1 --margin 0.1  --eval_metric per_query --cuda_device ${id} >             ./drill_down/Gplus_1_0.11_0.08_subgraph_ind_nodes_001_ranking_GIN_per_query_0.1_drilldown 2>&1 &

echo $! $id $count ; sleep 4m

free_mem=$(nvidia-smi --query-gpu=memory.free --format=csv -i $id | grep -Eo [0-9]+)
while [ $free_mem -lt 6000 ]; do
    id=$(( count % 3 ))
    free_mem=$(nvidia-smi --query-gpu=memory.free --format=csv -i $id | grep -Eo [0-9]+)

    if [[ $count -eq 3 ]]
    then
        id=0
        count=0
        sleep 2m
    fi;

    count=$[$count +1]
    sleep 5
done
python main.py --dataset Gplus_1 --batch_size 64   --tfrac 0.11 --vfrac 0.08              --use_n2v 1 --use_attribute 0 --use_tag 0             --uv_data  subgraph --input ind_nodes --model "GIN" --loss "ranking"  --hidden_channels 32 --num_layers 3 --last_hidden_size 128 --reqd_params -1 --margin 10.0  --eval_metric per_query --cuda_device ${id} >             ./drill_down/Gplus_1_0.11_0.08_subgraph_ind_nodes_100_ranking_GIN_per_query_10.0_drilldown 2>&1 &

echo $! $id $count ; sleep 4m

free_mem=$(nvidia-smi --query-gpu=memory.free --format=csv -i $id | grep -Eo [0-9]+)
while [ $free_mem -lt 6000 ]; do
    id=$(( count % 3 ))
    free_mem=$(nvidia-smi --query-gpu=memory.free --format=csv -i $id | grep -Eo [0-9]+)

    if [[ $count -eq 3 ]]
    then
        id=0
        count=0
        sleep 2m
    fi;

    count=$[$count +1]
    sleep 5
done
python main.py --dataset Twitter_3 --batch_size 64   --tfrac 0.115 --vfrac 0.08              --use_n2v 0 --use_attribute 0  --use_tag 1              --uv_data  subgraph --input combined --model "GCN" --loss "ranking"  --hidden_channels 32 --num_layers 3 --last_hidden_size 128 --reqd_params -1 --margin 0.1  --eval_metric per_query --cuda_device ${id} >             ./drill_down/Twitter_3_0.115_0.08_subgraph_combined_001_ranking_GCN_per_query_0.1_drilldown 2>&1 &

echo $! $id $count ; sleep 4m

free_mem=$(nvidia-smi --query-gpu=memory.free --format=csv -i $id | grep -Eo [0-9]+)
while [ $free_mem -lt 6000 ]; do
    id=$(( count % 3 ))
    free_mem=$(nvidia-smi --query-gpu=memory.free --format=csv -i $id | grep -Eo [0-9]+)

    if [[ $count -eq 3 ]]
    then
        id=0
        count=0
        sleep 2m
    fi;

    count=$[$count +1]
    sleep 5
done
python main.py --dataset Twitter_3 --batch_size 64   --tfrac 0.115 --vfrac 0.08              --use_n2v 1 --use_attribute 0 --use_tag 1             --uv_data  subgraph --input ind_nodes --model "GCN" --loss "ranking"  --hidden_channels 32 --num_layers 3 --last_hidden_size 128 --reqd_params -1 --margin 1.0  --eval_metric per_query --cuda_device ${id} >             ./drill_down/Twitter_3_0.115_0.08_subgraph_ind_nodes_101_ranking_GCN_per_query_1.0_drilldown 2>&1 &

echo $! $id $count ; sleep 4m

free_mem=$(nvidia-smi --query-gpu=memory.free --format=csv -i $id | grep -Eo [0-9]+)
while [ $free_mem -lt 6000 ]; do
    id=$(( count % 3 ))
    free_mem=$(nvidia-smi --query-gpu=memory.free --format=csv -i $id | grep -Eo [0-9]+)

    if [[ $count -eq 3 ]]
    then
        id=0
        count=0
        sleep 2m
    fi;

    count=$[$count +1]
    sleep 5
done
python main.py --dataset Twitter_3 --batch_size 64   --tfrac 0.115 --vfrac 0.08              --use_n2v 0 --use_attribute 0  --use_tag 1              --uv_data  subgraph --input ind_nodes --model "GIN" --loss "ranking"  --hidden_channels 32 --num_layers 3 --last_hidden_size 128 --reqd_params -1 --margin 0.1  --eval_metric per_query --cuda_device ${id} >             ./drill_down/Twitter_3_0.115_0.08_subgraph_ind_nodes_001_ranking_GIN_per_query_0.1_drilldown 2>&1 &

echo $! $id $count ; sleep 4m

free_mem=$(nvidia-smi --query-gpu=memory.free --format=csv -i $id | grep -Eo [0-9]+)
while [ $free_mem -lt 6000 ]; do
    id=$(( count % 3 ))
    free_mem=$(nvidia-smi --query-gpu=memory.free --format=csv -i $id | grep -Eo [0-9]+)

    if [[ $count -eq 3 ]]
    then
        id=0
        count=0
        sleep 2m
    fi;

    count=$[$count +1]
    sleep 5
done
python main.py --dataset Twitter_3 --batch_size 64   --tfrac 0.115 --vfrac 0.08              --use_n2v 1 --use_attribute 0 --use_tag 0             --uv_data  wholegraph --input ind_nodes --model "GIN" --loss "ranking"  --hidden_channels 32 --num_layers 3 --last_hidden_size 128 --reqd_params -1 --margin 0.1  --eval_metric per_query --cuda_device ${id} >             ./drill_down/Twitter_3_0.115_0.08_wholegraph_ind_nodes_100_ranking_GIN_per_query_0.1_drilldown 2>&1 &

echo $! $id $count ; sleep 4m

free_mem=$(nvidia-smi --query-gpu=memory.free --format=csv -i $id | grep -Eo [0-9]+)
while [ $free_mem -lt 6000 ]; do
    id=$(( count % 3 ))
    free_mem=$(nvidia-smi --query-gpu=memory.free --format=csv -i $id | grep -Eo [0-9]+)

    if [[ $count -eq 3 ]]
    then
        id=0
        count=0
        sleep 2m
    fi;

    count=$[$count +1]
    sleep 5
done
python main.py --dataset Twitter_3 --batch_size 64   --tfrac 0.115 --vfrac 0.08              --use_n2v 0 --use_attribute 0 --use_tag 1             --uv_data  subgraph --input combined --model "GCN" --loss "bce"  --hidden_channels 32 --num_layers 3 --last_hidden_size 128 --reqd_params -1 --margin -1  --eval_metric per_query --cuda_device ${id} >             ./drill_down/Twitter_3_0.115_0.08_subgraph_combined_001_bce_GCN_per_query_-1.0_drilldown 2>&1 &

echo $! $id $count ; sleep 3m

free_mem=$(nvidia-smi --query-gpu=memory.free --format=csv -i $id | grep -Eo [0-9]+)
while [ $free_mem -lt 6000 ]; do
    id=$(( count % 3 ))
    free_mem=$(nvidia-smi --query-gpu=memory.free --format=csv -i $id | grep -Eo [0-9]+)

    if [[ $count -eq 3 ]]
    then
        id=0
        count=0
        sleep 2m
    fi;

    count=$[$count +1]
    sleep 5
done
python main.py --dataset Twitter_3 --batch_size 64   --tfrac 0.115 --vfrac 0.08              --use_n2v 1 --use_attribute 0 --use_tag 1             --uv_data  subgraph --input ind_nodes --model "GCN" --loss "bce"  --hidden_channels 32 --num_layers 3 --last_hidden_size 128 --reqd_params -1 --margin -1  --eval_metric per_query --cuda_device ${id} >             ./drill_down/Twitter_3_0.115_0.08_subgraph_ind_nodes_101_bce_GCN_per_query_-1.0_drilldown 2>&1 &

echo $! $id $count ; sleep 3m

free_mem=$(nvidia-smi --query-gpu=memory.free --format=csv -i $id | grep -Eo [0-9]+)
while [ $free_mem -lt 6000 ]; do
    id=$(( count % 3 ))
    free_mem=$(nvidia-smi --query-gpu=memory.free --format=csv -i $id | grep -Eo [0-9]+)

    if [[ $count -eq 3 ]]
    then
        id=0
        count=0
        sleep 2m
    fi;

    count=$[$count +1]
    sleep 5
done
python main.py --dataset Twitter_3 --batch_size 64   --tfrac 0.115 --vfrac 0.08              --use_n2v 0 --use_attribute 0 --use_tag 1             --uv_data  subgraph --input combined --model "GIN" --loss "bce"  --hidden_channels 32 --num_layers 3 --last_hidden_size 128 --reqd_params -1 --margin -1  --eval_metric per_query --cuda_device ${id} >             ./drill_down/Twitter_3_0.115_0.08_subgraph_combined_001_bce_GIN_per_query_-1.0_drilldown 2>&1 &

echo $! $id $count ; sleep 3m

free_mem=$(nvidia-smi --query-gpu=memory.free --format=csv -i $id | grep -Eo [0-9]+)
while [ $free_mem -lt 6000 ]; do
    id=$(( count % 3 ))
    free_mem=$(nvidia-smi --query-gpu=memory.free --format=csv -i $id | grep -Eo [0-9]+)

    if [[ $count -eq 3 ]]
    then
        id=0
        count=0
        sleep 2m
    fi;

    count=$[$count +1]
    sleep 5
done
python main.py --dataset Twitter_3 --batch_size 64   --tfrac 0.115 --vfrac 0.08              --use_n2v 1 --use_attribute 0 --use_tag 1             --uv_data  subgraph --input ind_nodes --model "GIN" --loss "bce"  --hidden_channels 32 --num_layers 3 --last_hidden_size 128 --reqd_params -1 --margin -1  --eval_metric per_query --cuda_device ${id} >             ./drill_down/Twitter_3_0.115_0.08_subgraph_ind_nodes_101_bce_GIN_per_query_-1.0_drilldown 2>&1 &

echo $! $id $count ; sleep 3m

free_mem=$(nvidia-smi --query-gpu=memory.free --format=csv -i $id | grep -Eo [0-9]+)
while [ $free_mem -lt 6000 ]; do
    id=$(( count % 3 ))
    free_mem=$(nvidia-smi --query-gpu=memory.free --format=csv -i $id | grep -Eo [0-9]+)

    if [[ $count -eq 3 ]]
    then
        id=0
        count=0
        sleep 2m
    fi;

    count=$[$count +1]
    sleep 5
done
python main.py --dataset Gplus_1 --batch_size 64   --tfrac 0.11 --vfrac 0.08              --use_n2v 0 --use_attribute 0 --use_tag 1             --uv_data  subgraph --input combined --model "GCN" --loss "bce"  --hidden_channels 32 --num_layers 3 --last_hidden_size 128 --reqd_params -1 --margin -1  --eval_metric per_query --cuda_device ${id} >             ./drill_down/Gplus_1_0.11_0.08_subgraph_combined_001_bce_GCN_per_query_-1.0_drilldown 2>&1 &

echo $! $id $count ; sleep 3m

free_mem=$(nvidia-smi --query-gpu=memory.free --format=csv -i $id | grep -Eo [0-9]+)
while [ $free_mem -lt 6000 ]; do
    id=$(( count % 3 ))
    free_mem=$(nvidia-smi --query-gpu=memory.free --format=csv -i $id | grep -Eo [0-9]+)

    if [[ $count -eq 3 ]]
    then
        id=0
        count=0
        sleep 2m
    fi;

    count=$[$count +1]
    sleep 5
done
python main.py --dataset Gplus_1 --batch_size 64   --tfrac 0.11 --vfrac 0.08              --use_n2v 1 --use_attribute 0 --use_tag 1             --uv_data  subgraph --input ind_nodes --model "GCN" --loss "bce"  --hidden_channels 32 --num_layers 3 --last_hidden_size 128 --reqd_params -1 --margin -1  --eval_metric per_query --cuda_device ${id} >             ./drill_down/Gplus_1_0.11_0.08_subgraph_ind_nodes_101_bce_GCN_per_query_-1.0_drilldown 2>&1 &

echo $! $id $count ; sleep 3m

free_mem=$(nvidia-smi --query-gpu=memory.free --format=csv -i $id | grep -Eo [0-9]+)
while [ $free_mem -lt 6000 ]; do
    id=$(( count % 3 ))
    free_mem=$(nvidia-smi --query-gpu=memory.free --format=csv -i $id | grep -Eo [0-9]+)

    if [[ $count -eq 3 ]]
    then
        id=0
        count=0
        sleep 2m
    fi;

    count=$[$count +1]
    sleep 5
done
python main.py --dataset Gplus_1 --batch_size 64   --tfrac 0.11 --vfrac 0.08              --use_n2v 0 --use_attribute 0 --use_tag 1             --uv_data  subgraph --input combined --model "GIN" --loss "bce"  --hidden_channels 32 --num_layers 3 --last_hidden_size 128 --reqd_params -1 --margin -1  --eval_metric per_query --cuda_device ${id} >             ./drill_down/Gplus_1_0.11_0.08_subgraph_combined_001_bce_GIN_per_query_-1.0_drilldown 2>&1 &

echo $! $id $count ; sleep 3m

free_mem=$(nvidia-smi --query-gpu=memory.free --format=csv -i $id | grep -Eo [0-9]+)
while [ $free_mem -lt 6000 ]; do
    id=$(( count % 3 ))
    free_mem=$(nvidia-smi --query-gpu=memory.free --format=csv -i $id | grep -Eo [0-9]+)

    if [[ $count -eq 3 ]]
    then
        id=0
        count=0
        sleep 2m
    fi;

    count=$[$count +1]
    sleep 5
done
python main.py --dataset Gplus_1 --batch_size 64   --tfrac 0.11 --vfrac 0.08              --use_n2v 1 --use_attribute 0 --use_tag 1             --uv_data  subgraph --input combined --model "GIN" --loss "bce"  --hidden_channels 32 --num_layers 3 --last_hidden_size 128 --reqd_params -1 --margin -1  --eval_metric per_query --cuda_device ${id} >             ./drill_down/Gplus_1_0.11_0.08_subgraph_combined_101_bce_GIN_per_query_-1.0_drilldown 2>&1 &

echo $! $id $count ; sleep 3m
