import sys
import os.path
import argparse,random
from utils import *
from models import *
from datalists import *
from loss_functions import *
from initial_features import get_freezed_embed
from evaluation_protocols import  evaluate
from datasplits import get_splits_from_file
from early_stopping import EarlyStopping

parser = argparse.ArgumentParser(description='Link Prediction with SEAL')

# general settings
parser.add_argument('--dataset', default=None, help='network name')
parser.add_argument('--batch_size', type=int, default=50)
parser.add_argument('--epochs', default=1000,type=int)
parser.add_argument('--cuda_device',type=int,default=0,
                    help='the cuda device')
parser.add_argument('--lr', type=float,default=1e-3)
parser.add_argument('--num_workers', type=int,default=0, help="for dataloader, todo: make appropriate changes if reqd")
parser.add_argument('--seed', type=int,default=1000, help="random seed for splits generation")

#train-val-test frac, also tmin
parser.add_argument('--tfrac', default=None, type=float,help='test fraction')
parser.add_argument('--vfrac', default=None, type=float,help='fraction of whole-train which will be val')
parser.add_argument('--tmin', default=1, type=int,help='threshold value of triangles for nodes to qualify query nodes')
parser.add_argument('--splitfile', default=None, help='file which contains data split')
parser.add_argument('--datalistfile', default=None, help='file which contains data from which dataloaders have to be created')
parser.add_argument('--print_data', default=0, type=int,help='print the datasplit statistics and exit as well')

#which features to pass
parser.add_argument('--use_n2v', type=int,default=0,
                    help='whether to use node2vec node embeddings')
parser.add_argument('--use_nmf', type=int,default=0,
                    help='whether to use NMF node embeddings')
parser.add_argument('--use_attribute', type=int,default=0,
                    help='whether to use node attributes')
parser.add_argument('--use_tag', type=int,default=0,
                    help='whether to use Tag (drnl, etc)')
parser.add_argument('--tag_type', type=str,choices=["drnl","hop","zo","de","de+","degree"],default="drnl",
                    help='if using tags, then which ones')
parser.add_argument('--n2v_file', default=None,type=str)
parser.add_argument('--nmf_file', default=None,type=str)

#whether the information passed is limited to subgraphs around u-/-v or not
parser.add_argument('--uv_data', type=str,default=None,choices=["subgraph","wholegraph"],help="whether the information"
                                                                                              "is limited k-hop subgraph (subgraph) or the"
                                                                                              "whole graph (wholegraph)")
parser.add_argument('--hop', default=1, metavar='S',
                    help='enclosing subgraph hop number (or k), \
                    options: 1, 2,..., "auto"')

#whether the information passed is combined or not
parser.add_argument('--input', type=str,default=None,choices=["ind_nodes","combined"],help="if (ind_nodes) then the information for"
                                                                                           "u---v is such that information for u and v "
                                                                                           "that is passed is independent of each other, otherwise"
                                                                                           "if this is (combined) then the information of u--v is "
                                                                                           "such that the combined information from u and v is passed")
#model configurations
parser.add_argument('--model', type=str,default=None,choices=["DGCNN","GCN","SAGE","GIN"],help="the model to use")
parser.add_argument('--hidden_channels', type=int,nargs='+',help="# hidden channels, NOTE: All of the same size") #default is 32
parser.add_argument('--sortpool_k', type=float, default=0.6,help="This is k is necessary in DGCNN")
parser.add_argument('--num_layers', type=int, nargs='+',help="The number of hidden layers") #default is 3
parser.add_argument('--last_hidden_size', type=int, nargs='+',help="The size of final linear layer") #default=128
parser.add_argument('--reqd_params', type=int, default=-1,help="The number of reqd params, the actual number can be +-10%")

#which loss function to use
parser.add_argument('--loss', type=str,default=None,choices=["bce","ranking"],help="(bce) if want to use bce loss,(ranking)"
                                                                                   " if want to use ranking loss")
parser.add_argument('--margin', type=float,default=0.1,help='If loss=="ranking" then what margin to choose')

#which metric to use for early-stopping and also report for test performance
parser.add_argument('--eval_metric', type=str,default='per_query',choices=["per_query","global"],help='per-query or global eval metric')

args=parser.parse_args()
print(args)
process_args(args)

#Things to Note:
#1. "wholegraph" setting only works with "ind-nodes"
#2. in case of "wholegraph" atleast some other kind of features should be turned on- attributes or n2vembeddings and use_tag==0
#3. Options for modifying n2v embeddings are not acked in this argparse
# Few assertions to respect above constraints
assert args.uv_data!="wholegraph" or (args.use_tag==0 and (args.use_attribute==1 or args.use_n2v==1))
assert args.uv_data!="wholegraph" or args.input=="ind_nodes"

#create directories
os.makedirs("Data",exist_ok=True)
os.makedirs("Datalists",exist_ok=True)
os.makedirs("N2V",exist_ok=True)


random.seed(args.seed)
np.random.seed(args.seed)
torch.manual_seed(args.seed)
torch.cuda.manual_seed_all(args.seed)
# torch.backends.cudnn.enabled = False
# torch.backends.cudnn.benchmark = False
# torch.backends.cudnn.deterministic = True

A_csr,node_information=get_A_from_cites_file(args.dataset)
num_nodes=A_csr.shape[0]
query_nodes,  training_edge_list,  training_ne_list,  test_edge_list, \
test_ne_list,  val_edge_list,  val_ne_list = get_splits_from_file(args,A_csr)

train_pos=list(zip(*training_edge_list))
train_neg=list(zip(*training_ne_list))
val_pos=list(zip(*val_edge_list))
val_neg=list(zip(*val_ne_list))
test_pos=list(zip(*test_edge_list))
test_neg=list(zip(*test_ne_list))
print(len(train_pos[0]),len(train_neg[0]),len(val_pos[0]),len(val_neg[0]),len(test_pos[0]),len(test_neg[0]))

# eliminate test and val edges
A_csr[test_pos[0],test_pos[1]]=0
A_csr[test_pos[1],test_pos[0]]=0
A_csr[val_pos[0],val_pos[1]]=0
A_csr[val_pos[1],val_pos[0]]=0
A_csr.eliminate_zeros()

x,y=A_csr.nonzero()
A_csr[y,x]=1

train_query2posneg = {qnode: {"pos": [i for i, (u, v) in enumerate(training_edge_list) if u == qnode or v == qnode],
                              "neg": [i for i, (u, v) in enumerate(training_ne_list) if u == qnode or v == qnode]}
                      for qnode in query_nodes}
test_query2posneg = {qnode: {"pos": [i for i, (u, v) in enumerate(test_edge_list) if u == qnode or v == qnode],
                             "neg": [i for i, (u, v) in enumerate(test_ne_list) if u == qnode or v == qnode]}
                     for qnode in query_nodes}
val_query2posneg = {qnode: {"pos": [i for i, (u, v) in enumerate(val_edge_list) if u == qnode or v == qnode],
                            "neg": [i for i, (u, v) in enumerate(val_ne_list) if u == qnode or v == qnode]}
                    for qnode in query_nodes}
train_query2posneg={qnode: train_query2posneg[qnode] for  qnode in train_query2posneg if len(train_query2posneg[qnode]["pos"])>0 and
                                                                                             len(train_query2posneg[qnode]["neg"])>0}
val_query2posneg={qnode: val_query2posneg[qnode] for  qnode in val_query2posneg if len(val_query2posneg[qnode]["pos"])>0 and
                                                                                             len(val_query2posneg[qnode]["neg"])>0}
test_query2posneg={qnode: test_query2posneg[qnode] for  qnode in test_query2posneg if len(test_query2posneg[qnode]["pos"])>0 and
                                                                                             len(test_query2posneg[qnode]["neg"])>0}
train_edge_datalist,train_ne_datalist,\
val_edge_datalist,val_ne_datalist,\
test_edge_datalist,test_ne_datalist=get_datalists(args,training_edge_list,training_ne_list,val_edge_list,val_ne_list,
                                                  test_edge_list,test_ne_list,A_csr)

train_dl_shuffle,train_dl,val_dl,test_dl=get_dataloaders(args,train_edge_datalist,train_ne_datalist,
                                        val_edge_datalist,val_ne_datalist,
                                        test_edge_datalist,test_ne_datalist,
                                        train_query2posneg,val_query2posneg,test_query2posneg,"train")
(train_pos_dl_eval,train_neg_dl_eval),(val_pos_dl_eval,val_neg_dl_eval),(test_pos_dl_eval,test_neg_dl_eval)=get_dataloaders(args,train_edge_datalist,train_ne_datalist,
                                        val_edge_datalist,val_ne_datalist,
                                        test_edge_datalist,test_ne_datalist,
                                        train_query2posneg,val_query2posneg,test_query2posneg,"eval")

freezed_embed=get_freezed_embed(args,A_csr,node_information,val_edge_list,val_ne_list,val_query2posneg,train_pos,train_neg).to(args.device)


max_z=num_nodes if args.use_tag else None
if args.uv_data=="subgraph":
    num_nodes = None
    if args.input=="combined":
        num_nodes=[g.num_nodes for g in train_edge_datalist+train_ne_datalist+val_edge_datalist+val_ne_datalist]
    elif args.input=="ind_nodes":
        num_nodes=[g.num_nodes for x in train_edge_datalist+train_ne_datalist+val_edge_datalist+val_ne_datalist for g in x["uv"]]
wholegraph = None
if args.uv_data == "wholegraph":
    from torch_geometric.data import Data

    wholegraph = Data(None, torch.tensor(A_csr.nonzero()).long(), edge_weight=None,
                      node_id=torch.tensor(range(num_nodes)),
                      num_nodes=num_nodes)
table=[]
for hidden_size in args.hidden_channels:
    for num_hidden_layers in args.num_layers:
        for last_hidden_size in args.last_hidden_size:
            # if args.model == 'DGCNN':
            #     assert args.uv_data=="subgraph"
            #     model = DGCNN(hidden_size, num_hidden_layers, num_nodes,max_z,
            #                   freezed_embed,args.sortpool_k,last_hidden_size,dropout=0.5).to(args.device)
            # elif args.model == 'SAGE':
            #     model = GCN(hidden_size, num_hidden_layers,max_z,freezed_embed,last_hidden_size,dropout=0.5,gnn="sage").to(args.device)
            # elif args.model == 'GCN':
            #     model = GCN(hidden_size, num_hidden_layers,max_z,freezed_embed,last_hidden_size,dropout=0.5,gnn="gcn").to(args.device)
            # elif args.model == 'GIN':
            #     model = GIN(hidden_size, num_hidden_layers,max_z,freezed_embed,last_hidden_size,dropout=0.5,jk=True,
            #                 train_eps=False,device=args.device).to(args.device)
            #
            #
            # parameters = list(model.parameters())
            # total_params=sum(p.numel() for param in parameters for p in param)
            total_params=0

            if args.reqd_params>=0:
                if total_params<args.reqd_params*1.2 and total_params>args.reqd_params*0.8:
                    table.append([hidden_size,num_hidden_layers,last_hidden_size,total_params])
            else:
                table.append([hidden_size, num_hidden_layers, last_hidden_size, total_params])
# if len(table)<=1:
#     exit(0)


table=random.sample(table,min(len(table),5))
best_global_perf_among_configs=-np.inf
best_perquery_perf_among_configs=-np.inf

best_global_perf_config_among_configs=None
best_perquery_perf_config_among_configs=None

best_global_perf_epoch_among_configs=None
best_perquery_perf_epoch_among_configs=None

best_perquery_perf_quantities_among_configs=None
best_global_perf_quantities_among_configs=None

for hidden_size,num_hidden_layers,last_hidden_size,num_params in table:
    print(f"\033[90m current model configuration: hid_size %d, num_hid_lays %d, last_hid_size %d, num_params %d  \033[0m" %
          (hidden_size,num_hidden_layers,last_hidden_size,num_params))

    if args.model == 'DGCNN':
        assert args.uv_data == "subgraph"
        model = DGCNN(hidden_size, num_hidden_layers, num_nodes, max_z,
                      freezed_embed, args.sortpool_k, last_hidden_size, dropout=0.5).to(args.device)
    elif args.model == 'SAGE':
        model = GCN(hidden_size, num_hidden_layers, max_z, freezed_embed, last_hidden_size, dropout=0.5, gnn="sage").to(
            args.device)
    elif args.model == 'GCN':
        model = GCN(hidden_size, num_hidden_layers, max_z, freezed_embed, last_hidden_size, dropout=0.5, gnn="gcn").to(
            args.device)
    elif args.model == 'GIN':
        model = GIN(hidden_size, num_hidden_layers, max_z, freezed_embed, last_hidden_size, dropout=0.5, jk=True,
                    train_eps=False, device=args.device).to(args.device)

    parameters = list(model.parameters())
    optimizer = torch.optim.Adam(params=parameters, lr=args.lr)
    num_params = sum(p.numel() for param in parameters for p in param)
    start_epoch = 0
    earlystopping_local=EarlyStopping(-np.inf)
    earlystopping_global=EarlyStopping(-np.inf)
    quantities_local=None
    quantities_global=None
    best_epoch_global=None
    best_epoch_local=None
    # print(f'\033[91m The val score  of epoch {-1} is {compare_with:.4f} \033[0m')
    for epoch in range(start_epoch, start_epoch + args.epochs):
        if args.loss=="bce":
            epoch_loss=bce_loss(args, train_dl_shuffle, model, optimizer, wholegraph,"train")
        elif args.loss=="ranking":
            epoch_loss=ranking_loss(args, train_dl, model, optimizer, train_query2posneg, wholegraph,"train")

        auc,ap,qmap,qmrr=evaluate(args,wholegraph, val_pos_dl_eval,val_neg_dl_eval,model,val_query2posneg)
        earlystopping_global(ap, epoch,model)
        earlystopping_local(qmap, epoch,model)
        if earlystopping_global.improved==1:
            quantities_global=evaluate(args,wholegraph,test_pos_dl_eval,test_neg_dl_eval,model,test_query2posneg)
            best_epoch_global=epoch
        if earlystopping_local.improved==1:
            if earlystopping_global.improved: quantities_local=quantities_global
            else:
                quantities_local= evaluate(args, wholegraph, test_pos_dl_eval, test_neg_dl_eval, model,
                                             test_query2posneg)

            best_epoch_local=epoch
        if earlystopping_global.early_stop:
            earlystopping_global.stop_update=1
        if earlystopping_local.early_stop:
            earlystopping_local.stop_update=1

        if earlystopping_global.early_stop and earlystopping_local.early_stop:
            break
    if quantities_global[1]>best_global_perf_among_configs:
        best_global_perf_among_configs=quantities_global[1]
        best_global_perf_epoch_among_configs=best_epoch_global
        best_global_perf_config_among_configs=(hidden_size,num_hidden_layers,last_hidden_size,num_params)
        best_global_perf_quantities_among_configs=quantities_global
    if quantities_local[2]>best_perquery_perf_among_configs:
        best_perquery_perf_among_configs=quantities_global[2]
        best_perquery_perf_epoch_among_configs=best_epoch_local
        best_perquery_perf_config_among_configs=(hidden_size,num_hidden_layers,last_hidden_size,num_params)
        best_perquery_perf_quantities_among_configs=quantities_local




#the following three are based on best perquery eval
print(f"\033[95m Final Test evaluation------------Epoch:{best_perquery_perf_epoch_among_configs}------------- \033[0m")
print('\033[95m AUC %.5f AP %.5f qMAP %.5f qMRR %.5f\033[0m' % best_perquery_perf_quantities_among_configs)
print(f"\033[90m best perquery perf config: hid_size %d, num_hid_lays %d, last_hid_size %d, num_params %d \033[0m" % best_perquery_perf_config_among_configs)

#the following three are based on best global eval
print(f"\033[95m Final Test evaluation (according to global) ------------Epoch:{best_global_perf_epoch_among_configs}------------- \033[0m")
print('\033[95m AUC %.5f AP %.5f qMAP %.5f qMRR %.5f\033[0m' % best_global_perf_quantities_among_configs)
print(f"\033[90m best global perf config: hid_size %d, num_hid_lays %d, last_hid_size %d, num_params %d  \033[0m"% best_global_perf_config_among_configs)
