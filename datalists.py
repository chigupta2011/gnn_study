from data_u_v import extract_enclosing_subgraphs
import torch_geometric.data as tg_data
from torch.utils.data import  Dataset, TensorDataset, DataLoader

import torch,os,pickle as pkl
from tqdm import  tqdm
graphs_for_indnodes=None
class UVDatasetObj(object):
    def __init__(self, uv_idxs,y):
        self.uv=uv_idxs
        self.y=y

class IndNodesDatasetBatch(object):
    def __init__(self, indnodesdict):
        self.uv=indnodesdict["uv"]
        self.y=indnodesdict
def collate_fn_for_uvdataset(uvdatasetobjlist):
    uvlist,y=[],[]
    for i,obj in enumerate(uvdatasetobjlist):
        uvlist.append(obj.uv)
        y.append(obj.y)
    return torch.tensor(uvlist).long(),torch.tensor(y).float()

def collate_fn_for_indnodes(indnodes_dict):
    # usgraph=tg_data.Batch()
    # vsgraph=tg_data.Batch()
    glists=[]
    # num_nodes=[]
    for i in indnodes_dict:
        glists.append(i["uv"][0])
        # num_nodes.append(i["uv"][0].num_nodes)
    usgraph=tg_data.Batch.from_data_list(glists)
    # print(usgraph.num_graphs)
    # print(usgraph.to_data_list()[0])
    glists=[]
    for i in indnodes_dict:
        glists.append(i["uv"][1])
        # num_nodes.append(i["uv"][1].num_nodes)
    vsgraph=tg_data.Batch.from_data_list(glists)
    ys_list=[]
    for i in indnodes_dict:
        ys_list.append(i["y"])
    return (usgraph,vsgraph,torch.tensor(ys_list).float())

def datalists_for_list(args,uvlist,A_csr,y):
    global graphs_for_indnodes
    link_index=list(zip(*uvlist))
    # "ind_nodes", "combined", "subgraph", "wholeghraph"
    if args.uv_data=="subgraph":
        if args.input=="ind_nodes":
            if graphs_for_indnodes is None:
                graphs_for_indnodes=extract_enclosing_subgraphs(args,None, A_csr, None, None, args.hop,node_label=args.tag_type)
            data_list=[]
            for i in range(len(link_index[0])):
                src, dst = link_index[0][i], link_index[1][i]
                data_list.append({"uv":(graphs_for_indnodes[src],graphs_for_indnodes[dst]),"y":y})
            return data_list
        elif args.input=="combined":
            return extract_enclosing_subgraphs(args,link_index, A_csr, None, y, args.hop,node_label=args.tag_type)
    elif args.uv_data=="wholegraph":
        assert args.input=="ind_nodes"
        uvdataset=[]
        for i,_ in enumerate(uvlist):
            uvdataset.append(UVDatasetObj(uvlist[i],y))
        return uvdataset

def get_datalists(args,train_edge,train_nonedge,val_edge,val_nonedge,test_edge,test_nonedge,A_csr):
    if args.datalistfile is None:
        args.datalistfile=f"Datalists/{args.dataset}_tfrac_{args.tfrac}_vfrac_{args.vfrac}_tmin_{args.tmin}_UVdata_{args.uv_data}" \
            f"_input_{args.input}"
    if os.path.exists(args.datalistfile):
        with open(args.datalistfile,"rb") as file:
            temp=pkl.load(file)
        return temp

    temp=datalists_for_list(args,train_edge,A_csr,1),datalists_for_list(args,train_nonedge,A_csr,0),\
           datalists_for_list(args,val_edge,A_csr,1),datalists_for_list(args,val_nonedge,A_csr,0),\
           datalists_for_list(args,test_edge,A_csr,1),datalists_for_list(args,test_nonedge,A_csr,0)
    with open(args.datalistfile, "wb") as file:
        pkl.dump(temp,file)
    return temp

def get_dataloader_for_split(args,query2posneg,pos_datalist,neg_datalist,shuffle,mode):
    if args.uv_data == "subgraph":
        dler = tg_data.DataLoader
        if args.input=="ind_nodes":
            dler = lambda x, batch_size, shuffle, collate_fn=collate_fn_for_indnodes: DataLoader(x,
                                                                                                  batch_size=batch_size,
                                                                                                  shuffle=shuffle,
                                                                                                  collate_fn=collate_fn)

    elif args.uv_data == "wholegraph":
        dler = lambda x,batch_size,shuffle,collate_fn=collate_fn_for_uvdataset: DataLoader(x,batch_size=batch_size,shuffle=shuffle,
                                                                  collate_fn=collate_fn)
    if mode=="eval":
        return dler(pos_datalist, batch_size=args.batch_size, shuffle=shuffle), \
               dler(neg_datalist, batch_size=args.batch_size, shuffle=shuffle)

    elif args.loss=="ranking": #if mode is not eval than the mode is train
        return {qnode: {"pos":dler([pos_datalist[i] for i in query2posneg[qnode]["pos"]],
                                   batch_size=args.batch_size,shuffle=shuffle),
                        "neg":dler([neg_datalist[i] for i in query2posneg[qnode]["neg"]],
                                   batch_size=args.batch_size,shuffle=shuffle)}
                for qnode in query2posneg}
    elif args.loss=="bce": #if mode is not eval than the mode is train
        return dler(pos_datalist+neg_datalist,batch_size=args.batch_size,shuffle=shuffle)

def get_dataloaders(args,train_pos,train_neg,val_pos,val_neg,test_pos,test_neg,train2posneg,val2posneg,test2posneg,mode):
    if mode=="train":
        return [get_dataloader_for_split(args,train2posneg,train_pos,train_neg,True,mode),
                get_dataloader_for_split(args, train2posneg, train_pos,train_neg,False,mode),
                get_dataloader_for_split(args, val2posneg, val_pos, val_neg,False,mode),
                get_dataloader_for_split(args, test2posneg, test_pos, test_neg,False,mode)]
    elif mode=="eval":
        return [get_dataloader_for_split(args,train2posneg,train_pos,train_neg,False,mode),
                get_dataloader_for_split(args, val2posneg, val_pos, val_neg,False,mode),
                get_dataloader_for_split(args, test2posneg, test_pos, test_neg,False,mode)]

