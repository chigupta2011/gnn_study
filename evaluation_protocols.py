import numpy as np
import torch
from sklearn.metrics import roc_auc_score,average_precision_score
from loss_functions import get_model_output
def compute_scores(av,pos_scores,neg_scores,qnodes2posneg):

    all_node_ap = []
    all_qnode_ap = []
    all_node_rr = []
    all_qnode_rr = []
    qnode_set=set(qnodes2posneg.keys())
    qMap, qMrr=None,None
    if av.eval_metric=="per_query":
        for qnode in qnode_set:
            q_pos_scores=pos_scores[qnodes2posneg[qnode]["pos"]]
            q_neg_scores=neg_scores[qnodes2posneg[qnode]["neg"]]
            q_all_scores=np.concatenate((q_pos_scores,q_neg_scores))
            # print(len(pos_scores),len(neg_scores))
            q_all_labels = np.concatenate((np.ones(len(q_pos_scores)), np.zeros(len(q_neg_scores))))
            # print(q_all_labels)
            # print(q_all_scores)
            ap_score   = average_precision_score(q_all_labels, q_all_scores)
            so = np.argsort(q_all_scores)[::-1]
            labels_rearranged = q_all_labels[so]
            # print(labels_rearranged)
            rr_score = 1/(labels_rearranged.tolist().index(1)+1)
            all_node_ap.append(ap_score)
            all_node_rr.append(rr_score)
        qMap,qMrr=np.mean(all_node_ap), np.mean(all_node_rr)
    all_labels = np.hstack([np.ones(len(pos_scores)), np.zeros(len(neg_scores))])
    all_scores=np.concatenate((pos_scores,neg_scores))
    auc_score  = roc_auc_score(all_labels, all_scores)
    ap_score   = average_precision_score(all_labels, all_scores)
    if qMap is None: qMap=-1
    if qMrr is None: qMrr=-1

    return auc_score, ap_score,qMap,qMrr


def evaluate(args,wholegraph,pos_dataloader,neg_dataloader,model,qnode2posneg):
    model.eval()
    with torch.no_grad():
        pos_scores=[]
        neg_scores=[]
        for batch in pos_dataloader:
            temp,_=get_model_output(args,model,batch,wholegraph)
            pos_scores.append(temp)
        for batch in neg_dataloader:
            temp,_=get_model_output(args,model,batch,wholegraph)
            neg_scores.append(temp)
    return compute_scores(args,torch.cat(pos_scores).cpu().numpy(),
                          torch.cat(neg_scores).cpu().numpy(),
                          qnode2posneg)






