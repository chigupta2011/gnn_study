# Copyright (c) Facebook, Inc. and its affiliates.
# This source code is licensed under the MIT license found in the
# LICENSE file in the root directory of this source tree.

import math
import numpy as np
import torch
from torch.nn import (ModuleList, Linear, Conv1d, MaxPool1d, Embedding, ReLU,
                      Sequential, BatchNorm1d as BN)
import torch.nn.functional as F
from torch_geometric.nn import (GCNConv, SAGEConv, GINConv,
                                global_sort_pool, global_add_pool, global_mean_pool)
import pdb


class GCN(torch.nn.Module):
    def __init__(self, hidden_channels, num_layers,max_z,freezed_embed,last_hidden_size,dropout=0.5,gnn="gcn"):
        super(GCN, self).__init__()
        self.freezed_embed=freezed_embed
        self.max_z = max_z
        # self.z_embedding = Embedding(self.max_z, hidden_channels) if max_z is not None else None
        self.hidden_channenls=hidden_channels

        self.convs = ModuleList()
        initial_channels=freezed_embed.shape[1]+(hidden_channels if max_z is not None else 0)
        conv_layer=GCNConv if gnn=="gcn" else SAGEConv if gnn=="sage" else None
        self.gnn=gnn
        self.convs.append(conv_layer(initial_channels, hidden_channels))
        for _ in range(num_layers - 1):
            self.convs.append(conv_layer(hidden_channels, hidden_channels))

        self.dropout = dropout
        self.lin1 = Linear(hidden_channels, last_hidden_size)
        self.lin2 = Linear(last_hidden_size, 1)

    def reset_parameters(self):
        for conv in self.convs:
            conv.reset_parameters()

    def forward(self,x=None,edge_index=None, batch=None, edge_weight=None, node_id=None,uvs_index=None,ugraphs=None,vgraphs=None):
        # print("x",x)
        if uvs_index is not None:
            x=self.get_embedding(x=x,edge_index=edge_index, batch=batch, edge_weight=edge_weight, node_id=node_id,uvs_index=uvs_index)
            x=x[uvs_index[:,0]]*x[uvs_index[:,1]]
        elif ugraphs is not None:
            ux=self.get_embedding(x=ugraphs.z,edge_index=ugraphs.edge_index, batch=ugraphs.batch,
                                edge_weight=ugraphs.edge_weight, node_id=ugraphs.node_id)
            vx=self.get_embedding(x=vgraphs.z,edge_index=vgraphs.edge_index, batch=vgraphs.batch,
                                edge_weight=vgraphs.edge_weight, node_id=vgraphs.node_id)
            _, center_indices_u = np.unique(ugraphs.batch.cpu().numpy(), return_index=True)
            _, center_indices_v = np.unique(vgraphs.batch.cpu().numpy(), return_index=True)
            x=ux[center_indices_u]*vx[center_indices_v]
        else:
            x=self.get_embedding(x=x,edge_index=edge_index, batch=batch, edge_weight=edge_weight, node_id=node_id,uvs_index=uvs_index)
            _, center_indices = np.unique(batch.cpu().numpy(), return_index=True)
            x_src = x[center_indices]
            x_dst = x[center_indices + 1]
            x = (x_src * x_dst)
        x = F.relu(self.lin1(x))
        x = F.dropout(x, p=self.dropout, training=self.training)
        x = self.lin2(x)
        return x.view(-1)


    def get_embedding(self, x=None,edge_index=None, batch=None, edge_weight=None, node_id=None,uvs_index=None):
        # print(x.shape,self.z_embedding)
        if self.max_z is not None:
            z_embedding=torch.nn.functional.one_hot(torch.clamp(x,0,self.hidden_channenls-1),self.hidden_channenls)
            x=torch.cat((z_embedding,self.freezed_embed[node_id]),dim=1) #max_z is None when use_tag is None
        # if self.max_z is not None: x=torch.cat((self.z_embedding(x),self.freezed_embed[node_id]),dim=1) #max_z is None when use_tag is None
        else: x=self.freezed_embed[node_id]
        for conv in self.convs[:-1]:
            x = conv(x, edge_index, edge_weight) if self.gnn=="gcn" else conv(x, edge_index) if self.gnn=="sage" else None
            x = F.relu(x)
            x = F.dropout(x, p=self.dropout, training=self.training)
        x = self.convs[-1](x, edge_index, edge_weight) if self.gnn=="gcn" else self.convs[-1](x, edge_index) if self.gnn=="sage" \
            else None
        return x


# An end-to-end deep learning architecture for graph classification, AAAI-18.
class DGCNN(torch.nn.Module):
    def __init__(self, hidden_channels, num_layers, num_nodes,max_z,freezed_embed,k,last_hidden_size, gnn="gcn",dropout=0.5):
        super(DGCNN, self).__init__()
        self.freezed_embed=freezed_embed
        if k <= 1:  # Transform percentile to number.
            k = num_nodes[int(math.ceil(k * len(num_nodes))) - 1]
            k = max(10, k)
        self.k = int(k)
        self.max_z=max_z
        # self.z_embedding = Embedding(self.max_z, hidden_channels) if max_z is not None else None
        self.hidden_channenls=hidden_channels

        self.convs = ModuleList()
        initial_channels=freezed_embed.shape[1]+(hidden_channels if max_z is not None else 0)
        GNN=GCNConv if gnn=="gcn" else SAGEConv if gnn=="sage" else None


        self.convs.append(GNN(initial_channels, hidden_channels))
        for i in range(0, num_layers-1):
            self.convs.append(GNN(hidden_channels, hidden_channels))
        self.convs.append(GNN(hidden_channels, 1))


        conv1d_channels = [16, 32]
        total_latent_dim = hidden_channels * num_layers + 1
        conv1d_kws = [total_latent_dim, 5]
        self.conv1 = Conv1d(1, conv1d_channels[0], conv1d_kws[0],
                            conv1d_kws[0])
        self.maxpool1d = MaxPool1d(2, 2)
        self.conv2 = Conv1d(conv1d_channels[0], conv1d_channels[1],
                            conv1d_kws[1], 1)
        dense_dim = int((self.k - 2) / 2 + 1)
        dense_dim = (dense_dim - conv1d_kws[1] + 1) * conv1d_channels[1]
        self.lin1 = Linear(dense_dim, last_hidden_size)
        self.dropout=dropout
        self.lin2 = Linear(last_hidden_size, 1)

    def forward(self,x=None,edge_index=None, batch=None, edge_weight=None, node_id=None,uvs_index=None,ugraphs=None,vgraphs=None):
        if ugraphs is not None:
            ux=self.get_embedding(x=ugraphs.z,edge_index=ugraphs.edge_index, batch=ugraphs.batch,
                                edge_weight=ugraphs.edge_weight, node_id=ugraphs.node_id)
            vx=self.get_embedding(x=vgraphs.z,edge_index=vgraphs.edge_index, batch=vgraphs.batch,
                                edge_weight=vgraphs.edge_weight, node_id=vgraphs.node_id)
            x=ux*vx
        else:
            x=self.get_embedding(x=x,edge_index=edge_index, batch=batch, edge_weight=edge_weight, node_id=node_id,uvs_index=uvs_index)
        x = F.relu(self.lin1(x))
        x = F.dropout(x, p=self.dropout, training=self.training)
        x = self.lin2(x)
        return x.view(-1)

    def get_embedding(self, x,edge_index, batch, edge_weight=None, node_id=None,uvs_index=None):
        if self.max_z is not None:
            z_embedding=torch.nn.functional.one_hot(torch.clamp(x,0,self.hidden_channenls-1),self.hidden_channenls)
            x=torch.cat((z_embedding,self.freezed_embed[node_id]),dim=1) #max_z is None when use_tag is None

        # if self.max_z is not None: x=torch.cat((self.z_embedding(x),self.freezed_embed[node_id]),dim=1) #max_z is None when use_tag is None
        else: x=self.freezed_embed[node_id]
        xs = [x]

        for conv in self.convs:
            xs += [torch.tanh(conv(xs[-1], edge_index, edge_weight))]
        x = torch.cat(xs[1:], dim=-1)

        # Global pooling.
        x = global_sort_pool(x, batch, self.k)
        x = x.unsqueeze(1)  # [num_graphs, 1, k * hidden]
        x = F.relu(self.conv1(x))
        x = self.maxpool1d(x)
        x = F.relu(self.conv2(x))
        x = x.view(x.size(0), -1)  # [num_graphs, dense_dim]

        return x


class GIN(torch.nn.Module):
    def __init__(self, hidden_channels, num_layers, max_z,freezed_embed, last_hidden_size,dropout=0.5,
                 jk=True, train_eps=False,device=None):
        super(GIN, self).__init__()
        self.device=device
        self.freezed_embed=freezed_embed
        self.max_z=max_z
        # self.z_embedding = Embedding(self.max_z, hidden_channels) if max_z is not None else None
        self.hidden_channenls=hidden_channels

        self.jk = jk
        initial_channels=freezed_embed.shape[1]+(hidden_channels if max_z is not None else 0)

        self.conv1 = GINConv(
            Sequential(
                Linear(initial_channels, hidden_channels),
                ReLU(),
                Linear(hidden_channels, hidden_channels),
                ReLU(),
                BN(hidden_channels),
            ),
            train_eps=train_eps)
        self.convs = torch.nn.ModuleList()
        for i in range(num_layers - 1):
            self.convs.append(
                GINConv(
                    Sequential(
                        Linear(hidden_channels, hidden_channels),
                        ReLU(),
                        Linear(hidden_channels, hidden_channels),
                        ReLU(),
                        BN(hidden_channels),
                    ),
                    train_eps=train_eps))

        self.dropout = dropout
        self.out_embed_size=num_layers*hidden_channels if jk else hidden_channels
        if self.jk:
            self.lin1 = Linear(num_layers * hidden_channels, last_hidden_size)
            self.out_dim=last_hidden_size
        else:
            self.lin1 = Linear(hidden_channels, last_hidden_size)
            self.out_dim = last_hidden_size
        self.lin2 = Linear(self.out_dim, 1)

    def forward(self,x=None,edge_index=None, batch=None, edge_weight=None, node_id=None,uvs_index=None,ugraphs=None,vgraphs=None):
        if uvs_index is not None:

            x=self.get_embedding(x=x,edge_index=edge_index, batch=batch, edge_weight=edge_weight, node_id=node_id,uvs_index=uvs_index)
            x=x[uvs_index[:,0]]*x[uvs_index[:,1]]
        elif ugraphs is not None:
            ux=self.get_embedding(x=ugraphs.z,edge_index=ugraphs.edge_index, batch=ugraphs.batch,
                                edge_weight=ugraphs.edge_weight, node_id=ugraphs.node_id)
            vx=self.get_embedding(x=vgraphs.z,edge_index=vgraphs.edge_index, batch=vgraphs.batch,
                                edge_weight=vgraphs.edge_weight, node_id=vgraphs.node_id)
            # print(ux.shape)
            ux = global_mean_pool(ux, ugraphs.batch)
            vx = global_mean_pool(vx, vgraphs.batch)
            x=ux*vx
        else:
            x=self.get_embedding(x=x,edge_index=edge_index, batch=batch, edge_weight=edge_weight, node_id=node_id,uvs_index=uvs_index)
            x = global_mean_pool(x, batch)
        x = F.relu(self.lin1(x))
        x = F.dropout(x, p=self.dropout, training=self.training)
        x = self.lin2(x)
        return x.view(-1)

    def get_embedding(self, x,edge_index, batch, edge_weight=None, node_id=None,uvs_index=None):
        # print(x.shape,edge_index.shape)
        if len(node_id)==1: return torch.rand(1,self.out_embed_size).to(self.device)
        if self.max_z is not None:
            z_embedding=torch.nn.functional.one_hot(torch.clamp(x,0,self.hidden_channenls-1),self.hidden_channenls)
            x=torch.cat((z_embedding,self.freezed_embed[node_id]),dim=1) #max_z is None when use_tag is None

        # if self.max_z is not None: x=torch.cat((self.z_embedding(x),self.freezed_embed[node_id]),dim=1) #max_z is None when use_tag is None
        else: x=self.freezed_embed[node_id]
        x = self.conv1(x, edge_index)
        xs = [x]
        for conv in self.convs:
            x = conv(x, edge_index)
            xs += [x]
        return torch.cat(xs, dim=1) if self.jk else xs[-1]


