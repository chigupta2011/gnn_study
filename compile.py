import os
import argparse
import re
import csv

parser = argparse.ArgumentParser(description='Compile results')

# general settings
parser.add_argument('--base_dir', default=None, type=str, required=True, help='base_dir')
args=parser.parse_args()

tmp = args.base_dir.split('_')
dataset=None
last=0
tmp[0] = tmp[0].split('/')[-1]
if tmp[0]=="Twitter":
    dataset="Twitter_3"
    last+=2
elif tmp[0] == "Gplus":
    dataset = "Gplus_1"
    last += 2
else:
    dataset=tmp[0]
    last+=1
tfrac = tmp[last]; last+=1
vfrac = tmp[last]; last+=1
cmd="""#! /bin/bash
id=2
count=2
"""
repeating_cmd = """  
    id=$1
    count=0
    free_mem=$(nvidia-smi --query-gpu=memory.free --format=csv -i $id | grep -Eo [0-9]+)
#                        while [ $free_mem -lt 4000 ]; do
    while [[ ($free_mem -lt 5000  && $id -eq 0) || ($free_mem -lt 3000 && $id -ne 0) ]]; do
        if [[ $id -eq 1 ]]
        then
            id=2
        elif [[ $id -eq 2 ]]
        then
            id=0
        else
            id=0
        fi
        free_mem=$(nvidia-smi --query-gpu=memory.free --format=csv -i $id | grep -Eo [0-9]+)

        if [[ $count -eq 10 ]]
        then
            echo $pids
            #wait $pids || { echo "there were errors" >&2; } #exit 1; }
            pids=""
        fi;

        count=$[$count +1]
        sleep 5
    done
"""



headers=['uv_data','input type','nmf feature','n2v feature','attribute feature','tag feature','loss function',"eval","margin",'Model','pq_AUC','pq_AP', 'pq_qMAP', 'pq_qMRR','gl_AUC','gl_AP', 'gl_qMAP', 'gl_qMRR', '#Parameters']
headers_reverse_idx={y:x for (x,y) in enumerate(headers)}
results = [headers.copy()]
filelist = os.listdir(args.base_dir)
for file in filelist:
    filename= file
    if filename+"_rerun" in filelist:
        continue
    file = file.split('_')
    # tmp = [None for _ in range(len(headers))]
    tmp ={k:"####" for k in headers}
    last = 0
    tmp["uv_data"] = file[last]
    last+=1
    try:
        if file[1]=='ind':
            tmp["input type"] = file[last]+'_'+file[last+1]
            last+=2
        else:
            tmp["input type"] = file[last]
            last+=1
    except:
        print("File:",filename,"ignored")
        continue
    tmp["n2v feature"] = True if file[last][0]=='1' else False
    tmp["nmf feature"] = True if file[last][0]=='2' else False
    tmp["attribute feature"] = True if file[last][1]=='1' else False
    tmp["tag feature"] = True if file[last][2]=='1' else False
    last+=1
    tmp["loss function"] = file[last]; last+=1
    tmp["Model"] = file[last]; last+=1
    if file[last]=='per':
        tmp["eval"] = file[last]+'_'+file[last+1]; last+=2
    else:
        tmp["eval"] = file[last]; last+=1
    tmp["margin"] = file[last]; last+=1 #margin
    found_results = False
    with open(f'{args.base_dir}/{filename}') as f:
        pq_or_gl=0
        for line in f:
            prefx = "pq_" if pq_or_gl==0 else "gl_"
            if re.search('AUC', line):
                pq_or_gl=1
                found_results= True
                a=re.split('AUC | AP | qMAP | qMRR ', line)
                if a[1].strip()=='%.5f':
                    found_results= False
                    continue
                tmp[prefx+"AUC"] = a[1]; tmp[prefx+"AP"] = a[2]; tmp[prefx+"qMAP"]=a[3]
                tmp[prefx+"qMRR"] = a[4][:5]
            if re.search('best perquery perf config', line):
                tmp['#Parameters']=re.findall(r'\d+',re.split('num_params ',line)[-1])[0]



            # if re.search('Total number of parameters is ', line):
            #     tmp["#Parameters"] = re.split('Total number of parameters is ',line)[-1].strip()
    results.append([tmp[k] for k in headers])
    if not found_results:
        print(filename)
        cmd += repeating_cmd
        cmd += f'python main.py --dataset {dataset} --batch_size 64   --tfrac {tfrac} --vfrac {vfrac} \
               --use_n2v {1 if tmp["n2v feature"] else 0} --use_attribute {1 if tmp["attribute feature"] else 0} ' \
            f'--use_tag {1 if tmp["tag feature"] else 0} \
               --uv_data  {tmp["uv_data"]} --input {tmp["input type"]} --model "{tmp["Model"]}" --loss "{tmp["loss function"]}" ' \
            f' --hidden_channels 32 --num_layers 3 --last_hidden_size 128 ' \
            f'--reqd_params -1 --margin {tmp["margin"]}  --cuda_device $id > \
               {args.base_dir}/{tmp["uv_data"]}_{tmp["input type"]}_{1 if tmp["n2v feature"] else 0}{1 if tmp["attribute feature"] else 0}{1 if tmp["tag feature"] else 0}_' \
            f'{tmp["loss function"]}_{tmp["Model"]}_{tmp["eval"]}_{tmp["margin"]}_rerun 2>&1 \n' \
            f'echo  {args.base_dir}/{tmp["uv_data"]}_{tmp["input type"]}_{1 if tmp["n2v feature"] else 0}{1 if tmp["attribute feature"] else 0}{1 if tmp["tag feature"] else 0}_' \
            f'{tmp["loss function"]}_{tmp["Model"]}_{tmp["eval"]}_{tmp["margin"]}_rerun \n\n'
        cmd += "echo $! $id $count ; \n"
        # cmd += "echo $! $id $count ; sleep 4m \n"

cmd += "echo 'All done!' "
with open(f'{args.base_dir}/compile.csv','w') as f:
    writer = csv.writer(f, delimiter=',')
    writer.writerows(results)

with open(f'{args.base_dir}/rerun.sh','w') as f:
    f.write(cmd)





